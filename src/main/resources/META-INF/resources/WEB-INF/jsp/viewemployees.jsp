<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/main.css" rel="stylesheet">
</head>
<body>
	<h1 align="center">Students List</h1>
	<table id="t02" cellpadding="2">
		<tr>
			<th><a href="/enroll"><h2>Yeni Personel Kaydı</h2></a></th>
			<th><a align="right" href="/delete"><h2>Tüm Kayıtları Sil</h2></a></th>
		</tr>
	</table>

	<form:form class="form-horizontal">
		<table id="t01" border="2" width="70%" cellpadding="2">
			<tr>
				<th>Sicil No</th>
				<th>Ad</th>
				<th>Soyad</th>
				<th>Doğum Tarihi</th>
				<th>E-Posta</th>
				<th>Telefon</th>
				<th>Pin Kodu</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>

			<c:forEach var="employee" items="${list}">
				<tr>
					<td>${employee.empId}</td>
					<td>${employee.name}</td>
					<td>${employee.lastName}</td>
					<td>${employee.birthDate}</td>
					<td>${employee.email}</td>
					<td>${employee.mobileNo}</td>
					<td>${employee.pinCode}</td>
					<td><a href="/editstudent/${employee.empId}">Güncelle</a></td>
					<td><a href="/deletestudent/${employee.empId}">Sil</a></td>
				</tr>
			</c:forEach>


		</table>
		<br />


	</form:form>
</body>
</html>