<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Personel Kayıt Formu</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<script type="text/javascript">
	function hide(){
		var elem = document.getElementById('selection1');
		elem.style.visibility = 'hidden';
	}

	function show(){
		var elem = document.getElementById('selection2');
		elem.style.visibility = 'show';
	}
</script>
<body>
<div class="panel">
	<div class="panel-body">
		<div class="container">
			<h3>Çalışan Kayıt Formu</h3>
			<form:form method="POST" modelAttribute="employee" class="form-horizontal" action="save">
				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="name">Ad</label>
						<div class="col-md-7">
							<form:input type="text" path="name" id="name"
								class="form-control input-sm" />
							<div class="has-error">
								<form:errors path="name" class="help-inline" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="lastName">Soyad</label>
						<div class="col-md-7">
							<form:input type="text" path="lastName" id="lastName"
								class="form-control input-sm" />
							<div class="has-error">
								<form:errors path="lastName" class="help-inline" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="birthDate">Doğum
							Tarihi</label>
						<div class="col-md-7">
							<form:input type="text" path="birthDate" id="birthDate"
								class="form-control input-sm" />
							(yyyy-MM-dd)
							<div class="has-error">
								<form:errors path="birthDate" class="help-inline" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="email">E-Posta</label>
						<div class="col-md-7">
							<form:input type="text" path="email" id="email"
								class="form-control input-sm" />
							<div class="has-error">
								<form:errors path="email" class="help-inline" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="mobileNo">Telefon</label>
						<div class="col-md-7">
							<form:input type="text" path="mobileNo" id="mobileNo"
								class="form-control input-sm" />
							<div class="has-error">
								<form:errors path="mobileNo" class="help-inline" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-3 control-lable" for="pinCode"></label>
						<!-- Pin Kodu -->
						<div class="col-md-7">
							<form:input type="hidden" path="pinCode" id="pinCode"
								class="form-control input-sm" readonly="true" />
							<div class="has-error">
								<form:errors path="pinCode" class="help-inline" />
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="stars" style="display:none;">Yıldız</label>
                <div class="col-md-7">
                    <form:select path="stars" disabled="false" style="display:none;"
                    items="${stars}" multiple="true" itemValue="stars" 
                    itemLabel="type" class="form-control input-sm" />
                    <div class="has-error">
                        <form:errors path="stars" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

				<div class="row">
<!-- 					<div class="form-actions floatRight"> -->
					<input type="submit" value="Kaydet" class="btn btn-primary">
<!-- 					</div> -->
				</div>

			</form:form>
		</div>
	</div>
</div>

</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</html>