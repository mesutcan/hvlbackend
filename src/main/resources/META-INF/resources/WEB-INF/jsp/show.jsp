<%@ page session="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<!-- <link href="/css/custom.css" rel="stylesheet"> -->
<!-- <link href="/css/main.css" rel="stylesheet"> -->

</head>
<body>

	<div class="container">
		<div class="panel panel-success">
			<div class="alert alert-success" role="alert">
			<h4 class="alert-heading">Tebrikler!!</h4>
			<p>Kayıt işlemi başarılı şekilde tamamlanmıştır. Aşağıda personele ait pin kodu ve sicil numarası mevcuttur.</p>
	  		<hr>
			<br />
	
			
			<div class="row">
				<label class="col-sm-2">Sicil Numarası</label>
				<div class="col-sm-10">${employee.empId}</div>
			</div>
	
			<div class="row">
				<label class="col-sm-2">Pin Kodu</label>
				<div class="col-sm-10">${employee.pinCode}</div>
			</div>
			</div>
		</div>
	</div>

</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</html>
