package com.havelsan.hvlapp.repository;

import com.havelsan.hvlapp.entity.GateRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GateRecordRepository extends CrudRepository<GateRecord,Integer> {

    List<GateRecord> findGateRecordsByGateTypeAndPersonId(String gateType, int personId);

    List<GateRecord> findGateRecordsByGateTypeAndPersonIdAndDateIsGreaterThan(String gateType, String personId,long date);
}
