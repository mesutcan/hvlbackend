/**
 * 
 */
package com.havelsan.hvlapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.havelsan.hvlapp.entity.Star;


/**
 * @author anil
 *
 */
public interface StarRepository extends CrudRepository<Star, Integer>{

	
}
