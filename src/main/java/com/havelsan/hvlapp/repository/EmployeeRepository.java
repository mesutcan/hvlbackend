package com.havelsan.hvlapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.havelsan.hvlapp.dto.Point;
import com.havelsan.hvlapp.entity.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>{

	/**
	 * @param empId
	 * @return
	 */
	public Employee getEmployeeByEmpId(Integer empId);
	
//	public Employee saveEmployeeWithPoint(Employee employee, Point point);
	

}
