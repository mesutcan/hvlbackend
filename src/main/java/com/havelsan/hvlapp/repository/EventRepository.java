package com.havelsan.hvlapp.repository;

import com.havelsan.hvlapp.entity.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Integer> {

    List<Event> findByTime(long time);
}
