package com.havelsan.hvlapp.repository;

import com.havelsan.hvlapp.entity.Queue;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QueueRepository extends CrudRepository<Queue, Integer> {

    @Modifying
    @Query("delete from Queue q where q.employee.empId =:empId")
    void deleteAllByEmployeeId(Integer empId);

    @Query("SELECT q FROM Queue q WHERE q.employee.empId =:empId ORDER by q.time")
    Optional<List<Queue>> getAllByEmployeeId(Integer empId);
}
