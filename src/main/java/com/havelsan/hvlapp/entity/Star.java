/**
 * 
 */
package com.havelsan.hvlapp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author anil
 *
 */
@Entity
@Table(name ="tbl_star")
public class Star implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="POINT_ID")
	private Integer pointId;
	
	@Column(name ="POINT")
	private Integer point;
	
	@Column(name ="QUANTITY")
	private Integer quantity;
	
	@Column(name ="TYPE")
	private String type;
	
	@Temporal(TemporalType.DATE)
	@Column(name ="EARNED_TIME")
	private Date earnedTime;

	public Star() {
		
	}
	
	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPointId() {
		return pointId;
	}

	public void setPointId(Integer pointId) {
		this.pointId = pointId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getEarnedTime() {
		return earnedTime;
	}

	public void setEarnedTime(Date earnedTime) {
		this.earnedTime = earnedTime;
	}

	@Override
	public String toString() {
		return "Point [point=" + point + ", quantity=" + quantity + ", type=" + type + ", earnedTime=" + earnedTime
				+ "]";
	}
	
}
