/**
 * 
 */
package com.havelsan.hvlapp.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.havelsan.hvlapp.util.PinCodeGenerator;

/**
 * @author anil
 *
 */
@Entity
@Table(name = "tbl_employee")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMP_ID")
	private Integer empId;

	@Column(name = "NAME", length = 50)
	private String name;

	@Column(name = "LAST_NAME", length = 50)
	private String lastName;

	@Column(name = "BIRTH_DATE")
	private String birthDate;

	@Column(name = "PIN_CODE", length = 15)
	private String pinCode;

	@Column(name = "EMAIL", unique = true)
	private String email;

	@Column(name = "MOBILE_NO")
	private String mobileNo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "STAR_ID")
	private List<Star> stars = new ArrayList<>();

	public Employee() {

	}

	public List<Star> getStars() {
		return stars;
	}

	public void setStars(List<Star> stars) {
		this.stars = stars;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		pinCode = PinCodeGenerator.getAlphaNumericString(6);
		this.pinCode = pinCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return String.format(
				"Employee [empId=%s, name=%s,lastName=%s, birthDate=%s, " + "pinCode=%s,email=%s,mobileNo=%s]", empId,
				name, lastName, birthDate, pinCode, email, mobileNo);
	}

}
