/**
 * 
 */
package com.havelsan.hvlapp.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author anil
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

// Authentication : User --> Roles
	protected void configure(AuthenticationManagerBuilder auth)throws Exception {
		//		auth.inMemoryAuthentication().withUser("user1").password("secret1")
//				.roles("USER").and().withUser("admin1").password("secret1")
//				.roles("USER", "ADMIN");
	}

// Authorization : Role -> Access
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/*").permitAll();
//		http.httpBasic().and().authorizeRequests().antMatchers("/students/**")
//				.hasRole("USER").antMatchers("/**").hasRole("ADMIN").and()
//				.csrf().disable().headers().frameOptions().disable();
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
        .antMatchers(HttpMethod.GET)
        .antMatchers("/favicon.ico", "/", "/index.html", "/registrar","/**",
                "/autenticar", "/app/**");
        web.ignoring()
        .antMatchers(HttpMethod.POST)
        .antMatchers("/**","/");
        web.ignoring()
        .antMatchers(HttpMethod.PUT)
        .antMatchers("/**","/");
        web.ignoring()
        .antMatchers(HttpMethod.HEAD)
        .antMatchers("/**","/");
        web.ignoring()
        .antMatchers(HttpMethod.POST)
        .antMatchers("/**","/");
    }

}
