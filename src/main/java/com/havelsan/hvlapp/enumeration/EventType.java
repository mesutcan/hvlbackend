package com.havelsan.hvlapp.enumeration;

public enum EventType {
    BIRTH_DATE,
    TRAINING,
    ANNUAL_PAID_LEAVE
}