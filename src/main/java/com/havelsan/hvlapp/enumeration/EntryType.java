package com.havelsan.hvlapp.enumeration;

public enum EntryType {
    ENTER,
    EXIT
}
