package com.havelsan.hvlapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.service.EmployeeService;


@SpringBootApplication
public class HvlApp {

    public static void main(String[] args) {
        SpringApplication.run(HvlApp.class, args);
    }
    
    @Bean
    CommandLineRunner runner(EmployeeService employeeService) {
    	return args ->{
//    		//read json and write to db
//    		ObjectMapper mapper = new ObjectMapper();
//    		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//    		TypeReference<List<Employee>> typeReference = new TypeReference<List<Employee>>(){};
//    		InputStream inputStream = TypeReference.class.getResourceAsStream("/employees.json");
//    		try {
//				List<Employee> emps =mapper.readValue(inputStream, typeReference);
//				employeeService.save(emps);
//				System.out.println("Employee Saved!");
//			} catch (IOException e) {
//				System.out.println("Failed!"+e.getMessage());
//			} 
    	};
    }
}
