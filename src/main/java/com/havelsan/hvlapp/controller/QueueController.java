package com.havelsan.hvlapp.controller;

import com.havelsan.hvlapp.entity.Queue;
import com.havelsan.hvlapp.service.QueueService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/queue")
public class QueueController {

    private QueueService queueService;

    public QueueController(QueueService queueService) {
        this.queueService = queueService;
    }

    @GetMapping("/add")
    public int addToQueue(@RequestParam int empId) {
        return queueService.save(empId);
    }

    @GetMapping("/remove")
    public boolean removeFromQueue(@RequestParam int empId) {
        return queueService.delete(empId);
    }

    @GetMapping("/getMyOrder")
    public int getMyOrder(@RequestParam int empId) {
        return queueService.getMyOrder(empId);
    }

    @GetMapping("/getList")
    public List<Queue> getQueueList() {
        return queueService.getQueueList();
    }


}
