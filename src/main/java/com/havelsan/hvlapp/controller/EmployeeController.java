package com.havelsan.hvlapp.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.havelsan.hvlapp.dto.WelcomePageRequestObject;
import com.havelsan.hvlapp.dto.WelcomePageResponseObject;
import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.service.EmployeeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/employee")
@RequiredArgsConstructor
public class EmployeeController {

	private EmployeeService empService;

	@Autowired
	public EmployeeController(EmployeeService empService) {
		this.empService = empService;
	}

	@PostMapping(value = "/gate/{qrcode}")
	public ResponseEntity<String> controlGate(@RequestBody Object object) {
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@GetMapping("/list")
	public Iterable<Employee> list() {
		return empService.list();
	}

	@PostMapping(value = "/welcomePage")
	public ResponseEntity<WelcomePageResponseObject> welcomePage(
			@RequestBody WelcomePageRequestObject welcomePageRequestObject) {
		String responseMessage = "";
		WelcomePageResponseObject welcomePageResponseObject = new WelcomePageResponseObject();
		Optional<Employee> employee = empService.findById(welcomePageRequestObject.getId());
		if (employee.isPresent()) {
			responseMessage = "Hoşgeldiniz " + employee.get().getName() +" "+ employee.get().getLastName()+"";
			welcomePageResponseObject.setEmployee(employee.get());
			welcomePageResponseObject.setMessage(responseMessage);
			return new ResponseEntity<>(welcomePageResponseObject, HttpStatus.OK);
		}
		responseMessage = "Kullanıcı Bulunamadı";
		welcomePageResponseObject.setMessage(responseMessage);
		return new ResponseEntity<>(welcomePageResponseObject, HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/{empId}")
	public Employee retrieveEmployees(@PathVariable(name ="empId") Integer empId){
		return empService.getEmployeeByEmpId(empId);
	}
	
}