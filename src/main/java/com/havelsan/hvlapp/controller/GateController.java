package com.havelsan.hvlapp.controller;

import com.google.zxing.NotFoundException;
import com.havelsan.hvlapp.dto.Point;
import com.havelsan.hvlapp.entity.GateRecord;
import com.havelsan.hvlapp.enumeration.EntryType;
import com.havelsan.hvlapp.enumeration.GateType;
import com.havelsan.hvlapp.service.GateService;
import com.havelsan.hvlapp.service.QrCodeCacheService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(value = "/app")
public class GateController {

    @Autowired
    GateService gateService;

    @GetMapping("/emptySeat")
    public int getAllNews() {
        return gateService.getSeatCount();
    }

    @PostMapping("/saveGateRecord")
    public Point saveGateRecord(@RequestBody GateRecord gateRecord) throws NotFoundException{
        Point point = new Point();
        point.setPoint(0);
        gateRecord.setDate(System.currentTimeMillis());
        if (QrCodeCacheService.getSelfExpiringHashMap().containsKey(gateRecord.getQrId())) {
            gateService.saveGateRecord(gateRecord);
            boolean mainGate = GateType.MAIN.toString().equals(gateRecord.getGateType());
            boolean diningHallGate = GateType.DINING_HALL.toString().contentEquals(gateRecord.getGateType());
            boolean enter = EntryType.ENTER.toString().equals(gateRecord.getEntryType());
            boolean exit = EntryType.EXIT.toString().equals(gateRecord.getEntryType());

            Date date = new Date(gateRecord.getDate());

            if (mainGate) {
                if (enter && (date.getHours() >= 7 && date.getHours() <= 9)) {
                    point.setPoint(50);
                } else if (exit && (date.getHours() >= 17)) {
                    point.setPoint(50);
                }
                return point;

            }
            if (diningHallGate) {
                if (enter && (date.getHours() >= 12 && date.getHours() <= 13)) {
                    if (exit && (date.getMinutes() >= 1 && date.getMinutes() <= 10)) {
                        point.setPoint(100);
                    } else if (exit && (date.getMinutes() >= 11 && date.getMinutes() <= 15)) {
                        point.setPoint(50);
                    } else if (exit && (date.getMinutes() >= 16 && date.getMinutes() <= 20)) {
                        point.setPoint(25);
                    } else if (exit && (date.getMinutes() >= 21)) {
                        point.setPoint(0);
                    } else {
                        System.out.println("Herhangi bir puan kazanamadınız.");
                    }
                } else if (exit && (date.getHours() > 13)) {
                    System.out.println("Yemek saatleri 12:00 - 13:00 arasındadır.");
                }
            }
            return point;
        } else {
            throw NotFoundException.getNotFoundInstance();
        }
    }

    @GetMapping("/getGateRecords")
    public void getGateRecords(@RequestParam String gateType, @RequestParam int personId) {
        gateService.getGateRecords(gateType, personId);
    }

    @GetMapping("/calculateRemainingWorkingHour")
    public int calculateRemainingWorkingHour(@RequestParam int personId) {
        //return gateService.calculateRemainingWorkingHour(personId);
        return 4543;
    }

    @GetMapping("/calculateTotalWorkingHour")
    public int calculateTotalWorkingHour(@RequestParam int personId) {
       // return gateService.calculateTotalWorkingHour(personId);
        return 5466;
    }
}
