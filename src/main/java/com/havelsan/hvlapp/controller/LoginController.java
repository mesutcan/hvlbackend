/**
 * 
 */
package com.havelsan.hvlapp.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.havelsan.hvlapp.dto.Point;
import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.service.EmployeeService;

/**
 * @author anil
 *
 */
@Controller
public class LoginController {

	private EmployeeService empService;

	public LoginController(EmployeeService empService) {
		this.empService = empService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showLoginPage(ModelMap model) {
		model.put("name", "Anıl Erdoğan");
		return "home";
	}

	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("employee", new Employee());
		model.addAttribute("listEmployees", this.empService.list());
		return "employee";
	}

	@RequestMapping(value = "/enroll", method = RequestMethod.GET)
	public String newRegistration(ModelMap model) {
		Employee employee = new Employee();
		model.addAttribute("employee", employee);
		return "enroll";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveRegistration(@Valid Employee employee,Point point, BindingResult result, ModelMap model,
			RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "enroll";// will redirect to viewemp request mapping
		}
		
		empService.save(employee);
		
//		empService.saveEmployeeWithPoint(employee, point);
		System.out.println("Başlangıç Puanı: "+employee.getStars().toString());
		
		redirectAttributes.addFlashAttribute("message",
				"Çalışan Bilgisi: " + employee.getEmpId() + " " + employee.getPinCode() + " saved");
		return "redirect:/employees/" + employee.getEmpId(); // will redirect to viewemp request mapping
	}

	@RequestMapping(value = "/employees/{empId}", method = RequestMethod.GET)
	public String showEmployee(@PathVariable("empId") int empId, Model model) {

//		Employee emp = empService.findById(empId);
		Employee emp = empService.getEmployeeByEmpId(empId);
		if (emp == null) {
			System.out.println("Boş alanlar var.");
//			model.addAttribute("css", "danger");
//			model.addAttribute("msg", "User not found");
		}
		model.addAttribute("employee", emp);

		return "show";
	}


	@RequestMapping("/viewemployees")
	public ModelAndView viewstudents() {
		Iterable<Employee> employeeList = empService.list();
		return new ModelAndView("viewemployees", "list", employeeList);
	}
}
