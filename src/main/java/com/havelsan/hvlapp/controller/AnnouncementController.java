package com.havelsan.hvlapp.controller;

import com.havelsan.hvlapp.dto.News;
import com.havelsan.hvlapp.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/news")
public class AnnouncementController {

    @Autowired
    NewsService newsService;

    @GetMapping("/getAll")
    public List<News> getAllNews() {
        return newsService.getAllNews();
    }
}
