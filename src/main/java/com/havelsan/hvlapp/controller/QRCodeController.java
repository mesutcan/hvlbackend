package com.havelsan.hvlapp.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.havelsan.hvlapp.collection.SelfExpiringHashMap;
import com.havelsan.hvlapp.dto.Image;
import com.havelsan.hvlapp.dto.QRCode;
import com.havelsan.hvlapp.service.QRCodeService;
import com.havelsan.hvlapp.service.QrCodeCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;

@RestController
@RequestMapping(value = "/qrcode")
public class QRCodeController {

    public static int QR_CODE_TEXT;
    public static final long expirationTime = 100000;

    @Autowired
    QRCodeService qrCodeService;

    @PostMapping(value = "/generateQRCodeImage")
    private Image generateQRCodeImage(@RequestBody QRCode qrCode)
            throws WriterException {
        Random random = new Random();
        QR_CODE_TEXT = random.nextInt(1000000);
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        Image image = new Image();
        BitMatrix bitMatrix = qrCodeWriter.encode(Integer.toString(QR_CODE_TEXT)+","+qrCode.getId()+","+qrCode.getPinCode(), BarcodeFormat.QR_CODE, qrCode.getWidth(), qrCode.getHeight());
        BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix);
        QrCodeCacheService.getSelfExpiringHashMap().put(Integer.toString(QR_CODE_TEXT),qrCode.getId(),expirationTime);
        image.setBase64String(imgToBase64String(bufferedImage,"jpg"));
        return image;
    }

    public String imgToBase64String(final RenderedImage img, final String formatName) {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, formatName, Base64.getEncoder().wrap(os));
            return os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }
}
