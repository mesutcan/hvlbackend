package com.havelsan.hvlapp.service;

import com.havelsan.hvlapp.dto.News;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewsService {

    public List<News> getAllNews(){
        List<News> news = new ArrayList<>();
        prepareNewsData(news);
        return news;
    }

    private List<News> prepareNewsData(List<News> newsList){
        News news = new News("Uluslararası Büyük Veri Uygulamaları Konferansı başladı","https://media.licdn.com/dms/image/C4D22AQGjgbiA8avKQA/feedshare-shrink_8192/0?e=1564617600&v=beta&t=VJ0iGMnsu0n57M4Bki1eFCmFGOz--1gknriXG6Of2eM");
        News news1 = new News("ATAKSİM ÜLKEMİZE HAYIRLI OLSUN","https://media.licdn.com/dms/image/C4D22AQHsUJ0FAiVkbA/feedshare-shrink_8192/0?e=1564617600&v=beta&t=n0Pl7lEloEvxQFZWmsyW6f3DPHq7DjZLnPZc5US-mEU");
        News news2 = new News("5G üzerine her şey bugün BTK ev sahipliğinde TeknolojiSohbetleri4’te konuşuluyor.","https://media.licdn.com/dms/image/C4D22AQEVLbvy3-jR9A/feedshare-shrink_8192/0?e=1564617600&v=beta&t=c0p-jRly0CTcZDESkun4vaq7hdyNoI_0BJ3oeGrfIs4");
        News news3 = new News("Babalar Günü Kutlu Olsun","https://media.licdn.com/dms/image/C5622AQF5g4J7LJI1Ww/feedshare-shrink_8192/0?e=1564617600&v=beta&t=S4_Cv_BpPuLqFge_l5k94XVHqf0eP7Rz0hyvfN6U1dw");
        newsList.add(news);
        newsList.add(news1);
        newsList.add(news2);
        newsList.add(news3);
        return newsList;
    }
}
