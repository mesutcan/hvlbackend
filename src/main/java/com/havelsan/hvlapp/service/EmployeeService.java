package com.havelsan.hvlapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.havelsan.hvlapp.dto.Point;
import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.entity.Star;
import com.havelsan.hvlapp.repository.EmployeeRepository;

@Service
public class EmployeeService {

	private EmployeeRepository empRepository;

	public EmployeeService(EmployeeRepository empRepository) {
		this.empRepository = empRepository;
	}

	public Iterable<Employee> list() {
		return empRepository.findAll();
	}

	public Employee save(Employee emp) {
		List<Star> starList = new ArrayList<Star>();
		Star star = new Star();
		starList.add(star);
		emp.setStars(starList);
		return empRepository.save(emp);
	}

	public Iterable<Employee> save(List<Employee> employees) {
		return empRepository.saveAll(employees);
	}

	public Optional<Employee> findById(Integer empId) {
		return empRepository.findById(empId);
	}

	public void delete(int empId) {
		empRepository.deleteById(empId);
	}

	public Employee getEmployeeByEmpId(Integer empId) {
		return empRepository.getEmployeeByEmpId(empId);
	}

}
