package com.havelsan.hvlapp.service;

import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.entity.Queue;
import com.havelsan.hvlapp.repository.EmployeeRepository;
import com.havelsan.hvlapp.repository.QueueRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class QueueService {

    private QueueRepository queueRepository;
    private EmployeeRepository employeeRepository;

    public QueueService(QueueRepository queueRepository, EmployeeRepository employeeRepository) {
        this.queueRepository = queueRepository;
        this.employeeRepository = employeeRepository;
    }

    public Integer save(Integer empId) {
        Optional<Employee> byId = getEmployeeById(empId);
        if (byId.isPresent()) {
            Queue savedQueue = queueRepository.save(new Queue(System.currentTimeMillis(), byId.get()));
            return savedQueue.getId();
        } else {
            throw new IllegalArgumentException(empId + " bu id ile kayitli bir employee bulunamadı!");
        }
    }

    public boolean delete(Integer empId) {
        Optional<Employee> byId = getEmployeeById(empId);
        if (byId.isPresent()) {
            queueRepository.deleteAllByEmployeeId(byId.get().getEmpId());
            return true;
        } else {
            throw new IllegalArgumentException(empId + " bu id ile kayitli bir employee bulunamadı!");
        }

    }

    public List<Queue> getQueueList() {
        List<Queue> all = (List<Queue>) queueRepository.findAll();
        for (int i = 0; i < all.size(); i++) {
            Queue queue = all.get(i);
            queue.setOrder(i+1);
        }
        return all;
    }

    public int getMyOrder(Integer empId) {
        List<Queue> all = (List<Queue>) queueRepository.findAll();
        if (Objects.nonNull(all)) {
            Employee employeeByEmpId = employeeRepository.getEmployeeByEmpId(empId);
            for (int i = 0; i < all.size(); i++) {
                if (all.get(i).getEmployee().getEmpId() == employeeByEmpId.getEmpId()) {
                    return i+1;
                }
            }
        }
        throw new IllegalArgumentException(empId + " bu id ile kayitli epmloyee'nin queue bilgisi bulunamadı!");
    }

    private Optional<Employee> getEmployeeById(Integer employee_id) {
        return employeeRepository.findById(employee_id);
    }
}
