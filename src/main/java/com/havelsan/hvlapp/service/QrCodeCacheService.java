package com.havelsan.hvlapp.service;

import com.havelsan.hvlapp.collection.SelfExpiringHashMap;

public class QrCodeCacheService {

    private static SelfExpiringHashMap<String, String> selfExpiringHashMap = new SelfExpiringHashMap();

    public static SelfExpiringHashMap<String, String> getSelfExpiringHashMap() {
        return selfExpiringHashMap;
    }

    public static void setSelfExpiringHashMap(SelfExpiringHashMap<String, String> selfExpiringHashMap) {
        QrCodeCacheService.selfExpiringHashMap = selfExpiringHashMap;
    }
}
