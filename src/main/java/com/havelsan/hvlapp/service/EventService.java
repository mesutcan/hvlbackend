package com.havelsan.hvlapp.service;

import com.havelsan.hvlapp.entity.Event;
import com.havelsan.hvlapp.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public List<Event> getAllEvents() {
        List<Event> events = new ArrayList<>();
        eventRepository.findAll().iterator().forEachRemaining(event -> {
            events.add(event);
        });
        return events;
    }

    public List<Event> getByDate(long time) {
        return eventRepository.findByTime(time);
    }

}
