package com.havelsan.hvlapp.service;

import com.google.zxing.common.BitMatrix;
import com.havelsan.hvlapp.collection.SelfExpiringHashMap;
import org.springframework.stereotype.Component;

@Component
public class QRCodeService {

    private SelfExpiringHashMap<String, BitMatrix> qrCodeMap;

    public QRCodeService() {
        this.qrCodeMap = new SelfExpiringHashMap<>();
    }

    public SelfExpiringHashMap<String, BitMatrix> getQrCodeMap() {
        return qrCodeMap;
    }
}
