package com.havelsan.hvlapp.service;

import com.havelsan.hvlapp.entity.GateRecord;
import com.havelsan.hvlapp.enumeration.GateType;
import com.havelsan.hvlapp.repository.GateRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GateService {

    @Autowired
    GateRecordRepository gateRecordRepository;

    public int getSeatCount() {
        return 244;
    }

    public void saveGateRecord(GateRecord gateRecord) {
        gateRecordRepository.save(gateRecord);
    }

    public List<GateRecord> getGateRecords(String gateType, int personId) {
        return gateRecordRepository.findGateRecordsByGateTypeAndPersonId(gateType, personId);
    }

    public int calculateRemainingWorkingHour(String personId) {
        long remainingTime = 28800000;
        List<GateRecord> gateRecords = gateRecordRepository.findGateRecordsByGateTypeAndPersonIdAndDateIsGreaterThan
                (GateType.MAIN.toString(), personId, System.currentTimeMillis() - (1000L * 60 * 60 * 24));
        List<GateRecord> sortedList = gateRecords.stream()
                .sorted(Comparator.comparingLong(GateRecord::getDate))
                .collect(Collectors.toList());
        for (int i = 0; i <= sortedList.size(); i++) {

        }

        return 10;
    }

    public int calculateTotalWorkingHour(int personId) {

        return 10;
    }

}
