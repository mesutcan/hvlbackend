/**
 * 
 */
package com.havelsan.hvlapp.util;

import java.text.ParseException;
import java.util.Optional;

import com.havelsan.hvlapp.entity.Employee;
import com.havelsan.hvlapp.repository.EmployeeRepository;
import com.havelsan.hvlapp.service.EmployeeService;

/**
 * @author anil
 *
 */
public class Test {

	public static void main(String[] args) throws ParseException {

		Employee emp = new Employee();
		emp.setName("Mehmet");
		emp.setLastName("Yılmaz");
		emp.setMobileNo("321321432");
		emp.setEmail("myilmaz@test.com");
		emp.setPinCode(PinCodeGenerator.getAlphaNumericString(6));

		emp.setBirthDate("05-10-1997");
		
		System.out.println("Emp:" + emp);
	}
}
