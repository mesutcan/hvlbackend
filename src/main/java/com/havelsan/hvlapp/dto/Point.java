package com.havelsan.hvlapp.dto;

import java.io.Serializable;

public class Point implements Serializable {

    private int point;

    public Point() {

    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
