package com.havelsan.hvlapp.dto;

import com.havelsan.hvlapp.entity.Employee;

import java.io.Serializable;

public class WelcomePageResponseObject implements Serializable {

    private Employee employee;

    private String message;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
