package com.havelsan.hvlapp.dto;

import java.io.Serializable;

public class Image implements Serializable {

    private String base64String;

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }
}
