package com.havelsan.hvlapp.dto;

import java.io.Serializable;


public class WelcomePageRequestObject implements Serializable {

    private String pinNumber;

    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }
}
