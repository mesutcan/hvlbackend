package com.havelsan.hvlapp.dto;

import java.io.Serializable;

public class News implements Serializable {

    private String text;

    private String url;

    public News() {
    }

    public News(String text, String url) {
        this.text = text;
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
